$(document).ready(function () {
    var years = [2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015]; // Any year could be added to array and it will reflect in the app
        var championCall = []; // This is to store jSON Data for each year
        var driver = []; // Currently this is being used to store World Champions name and url only. We can store other driver information for that season if required.
        /**
         * My first attempt was running the for loop and dealing with the result directly.
         * This causes some issues as the response doesn't complete for each year before registering in DOM.
         * if the number of years increases in the years array, the problem is definitely significant.
         * Hence, the array first will gather all the jSON data from getJsonData function and store it in championCall array
        */
        for (i=0; i < years.length; i++) {
                championCall.push(getJsonData('champion', years[i]));
        }
        
        /**
         * This method will run only if the for loop above is completed and we have all the jSON Data in the array
        */
        $.when.apply($, championCall).done(function(){
            var html = "<table class='table table-hover'><thead><tr><th>Year</th><th>World Champion</th><th>Action</th></tr></thead>"; // html for table output
            
                for(var i = 0, len = arguments.length; i < len; i++){ // looping through each jSON year object
                        var worldChampion = arguments[i][0].MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver; // Finding the Driver node
                        // creating year array, this isn't necessary for one information only such as name,
                        // however if we need more information we can store them easily for that year world champion
                        driver[years[i]] = [];
                        driver[years[i]].name =  worldChampion.givenName + " "+ worldChampion.familyName; // Concat givenName and familyName, store it in name key
                        driver[years[i]].url = worldChampion.url;
                }
                // Once we have all the world champions for all the years given, populating table row is the next step
                for (var j = 0; j<years.length; j++){
                    html += "<tr><td>"+years[j]+"</td>"; // Display year
                    html += "<td>"+driver[years[j]].name+"<br/><a href='"+driver[years[j]].url+"' target='_blank'>Wikipedia Profile <i class='fa fa-external-link' aria-hidden='true'></i></a></td>"; // Display name, wikipedia link
                    // Add available action.
                    // Two html5 data attribute used to store current year and World Champion for that year
                    html += "<td class='action'><a href='#' data-champion='"+driver[years[j]].name+"' data-year='"+years[j]+"'>Winner of other races</a></td></tr>"; 
                }
            
                html += "</table>"; // closing table tag
                $("#data").html(html); // adding the html content to data div
        });
        
        
    /**
     * The following code block deals with action link for individual row.
     * .on(parameters) used to make sure the links are available to click
    */
    $(document).on('click', ".action a", function(e) {
        e.preventDefault();
        var year = $(this).attr('data-year'); // Retrieve year from data attribute data-year
        var champion = $(this).attr('data-champion'); // Retrieve World Champion from data attribute data-champion
        var id = "#results"; // We'll display all other race winners in this DIV
        var resultsHtml = ""; // html output string to store all the result before displaying
        resultsHtml += "<i class='fa fa-window-close fa-3x' aria-hidden='true' id='close'></i>"; // adding the close icon, css code to float it right corner
        resultsHtml += "<h2>Winner of other Championships in "+ year +"</h2><span class='label label-info'>* Highlighted winner is also the World Champion on the same season</span>";
        resultsHtml += "<table class='table table-hover'><thead><tr><th>Race Name</th><th>Winner</th></tr></thead><tbody>"; // Table header
        
        $.getJSON('http://ergast.com/api/f1/'+year+'/results/1.json', function(data){ // Retrieving json object for all race winners on that season
            var races = data.MRData.RaceTable.Races; // Finding the Races node
            $.each(races, function (key, val){ // Iterate through each node as key, value pair
                // Concat Driver (winner) full name
                // We only need the first element (Results[0]) as the position is 1, hence the winner of the race
                var driver = val.Results[0].Driver.givenName + " " + val.Results[0].Driver.familyName; 
                var classHighlight = champion==driver?"class='info'":""; // classHighlight will store class attribue to World Champion on that season to highlight that row
                
                resultsHtml += "<tr "+classHighlight+">"; // Add class to the row
                resultsHtml += "<td>"+val.raceName+"</td>"; // Race name
                resultsHtml += "<td>"+driver+"</td>"; // Winner
                resultsHtml += "</tr>";
            });
            resultsHtml += "</tbody></table>"; // Closing table tags
            //$(".results").hide();
            $(id).html(resultsHtml); // Adding content to the results div
            $(id).show("slow"); // Show the results div
            $("#data").addClass("blur"); // Blur the background data div (Not supported in IE, hence rgba background will be closest effect)
            $('html, body').animate({
                scrollTop: $( id ).offset().top-100 // scroll top for small/mobile browsers
            });

        });
      });
                  
    // Handling the results close icon
    $(document).on('click', "#close", function() {
        $("#results").hide('slow'); // Hide the result div
        $("#data").removeClass('blur'); // Remove blur effecr from data DIV
    });
                  
  });
          
/**
 * @param queryType and year
 * @returns JSON data
*/
function getJsonData(queryType, year) {
    var api = 'http://ergast.com/api/f1/';
    if(queryType == "champion"){
         api += year+'/driverStandings.json?limit=1'; // Limit 1 is to save processing time as we only care about the driver with most points
    }
    else{
        $("#data").html("<div class='alert alert-danger'>Invalid request</div>");
        return false;
    }
        return $.getJSON(api);  // this returns a "promise"
}