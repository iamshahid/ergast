 (function(){
    var ergast = angular.module('ergast', []); // ng-app module
    var api = "http://ergast.com/api/f1/"; // api base url
    var years = [2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015];// Any year could be added to array and it will reflect in the app
    var driver = []; // Array to store World Champion for each year
    
    /**
     * This controller has a function that takes year as a parameter and return winners for all championship races
     * Also, it will be called on body load to diplay for the first year in the array
     */
    ergast.controller('ErgastController', ['$scope','$http', function ($scope, $http){
        $scope.loadData = function(year){
            $http.get(api+year+'/results/1.json').then(function(response) { // formulate the api call and get jSON data response
            $scope.seasons = response.data; // Storing the response data in seasons scope variable
            $scope.activeYear = year; // storing current year for highlighting and results window
            },
            function error(response){
                console.log(response); // log error
            }
            );
        };
      $scope.years = years; // years array to run ng-loop
      $scope.driver = driver; // driver array to deal with any world champion related opeation such as highlight winner row
      $scope.loadData($scope.years[0]); // This will load the results for other races for the first year in the array
    }]);
    
    /**
     * This controller will generate World Champion data
     * The world champion information will be stored in driver array
     */
    
    ergast.controller('worldChampion', ['$scope','$http', function($scope, $http){ 
        
        $scope.years = years;
        $scope.years.forEach(function(value){ // Looping through each year element
            $http.get(api+value+'/driverStandings.json?limit=1').then(function(response) { // formulate api string and collect json data for position 1 only
               driverStandings = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver; // driver details node
               driver[value] = []; // creating 2 dimension array with year key
               // storing driver name, code and url in driver year key so that we can track for each year world champion details. More necessary information can be added
               driver[value].push({name: driverStandings.givenName + " " + driverStandings.familyName, code: driverStandings.code, url: driverStandings.url});
            });
        });
        // Once all the world champion is collected for years array, it will be passed as driver scope variable
        $scope.driver = driver;
    }]);
})();