# Ergast F1 world champions by year
---------------------------------

## Problem:
We will use http://ergast.com/mrd/ to create a single page application that presents a list that shows the F1 world champions starting from 2005 until 2015. Clicking on an item shows the list of the winners for every race for the selected year. We also request to highlight the row when the winner has been the world champion in the same season.

## Solution 1: Using jQuery
**How to run:**

  * Once downloaded please navigate to *jquery* folder and double click on index.html. It should open the app automatically in most of the modern browsers  
  * Internet Explorer may want you to click on "Allow blocked content" button  
  * The initial screen displays World Champion for given years  
  * User can view Winner of other races by clicking action link from the World Champion list and Winner of other Champions for that season will be displayed in an overlay box  
  * Click on the Exit/Close icon on the top right corner to close the results overlay and go back to the World Champion list  
  * Driver wikipedia profile link is available from World Champion column (open in a new tab/window)  

**The inline comments demonstrate the process.**
#### Files
  * index.html: html code for layout
  * custom.css: some customisation and UI fixing
  * app.js: main jquery code


## Solution 2: Using Angular
**How to run:**

  * Once downloaded please navigate to *angularjs* folder and double click on index.html. It should open the app automatically in most of the modern browsers  
  * Internet Explorer may want you to click on "Allow blocked content" button  
  * The screen is divided into two parts, left hand side list displays World Champion for given years and right hand side list displays Winner of other Champions in a selected year  
  * A right arrow would indicate which year the results are from  
  * User can view Winner of other races by clicking action link from the World Champion list and Winner of other Champions for that season will be displayed on the right hand side  
  * The app loads the data for first element in the year given  
  * Driver wikipedia profile link is available from World Champion column (open in a new tab/window)  

**The inline comments demonstrate the process.**
#### Files
  * index.html: html code for layout
  * custom.css: some customisation and UI fixing
  * app.js: main angular code
